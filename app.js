const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const keys = require("./config/keys").secretIOkey;
const db = require("./config/keys").mongoURI;


mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log('Database sucessfully connected');
}).catch((err) => {
    console.log(error);
});

app.use(
    bodyparser.urlencoded({
        extended: false
    })
);

app.use(bodyparser.json());

const companyRoutes = require('./server/routes/companyRoutes');
const userRoutes = require('./server/routes/userRoute');

app.use('/company', companyRoutes);
app.use('/user', userRoutes);

app.listen(8001, (req, res, next) => {
    console.log('Server Successfully Start');
})