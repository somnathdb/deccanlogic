const Company = require('../model/companyModel');
const UserModel = require('../model/userModel');


exports.delete_compnay = async (req, res, next) => {
    try {
        const body = req.body;
        console.log(body)
        var deleteRecord = await Company.findOneAndRemove({
            _id: body.id
        })
        console.log(deleteRecord)
        if (deleteRecord) {
            res.status(200).json({
                message: 'Company Successfully Delete'
            })
        }
    } catch (err) {
        console.log(err)
    }
}

exports.allocate_compnay = async (req, res, next) => {
    try {
        const body = req.body;
        console.log(body.id)
        var allocateCompany = await UserModel.find({
            company_id: body.id
        })
        console.log(allocateCompany)
        if (allocateCompany) {
            res.status(200).json({
                message: 'Company Successfully allocated'
            })
        }
    } catch (err) {
        res.status(200).json({
            message: "Something is wrong"
        })
    }
}