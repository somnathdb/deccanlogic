const userModel = require('../model/userModel');


exports.ListUser = async (req, res, next) => {
    const body = req.body;
    const add = new userModel({
        company_id: body.company_id,
        user_name: body.user_name,
        email: body.email,
        phone: body.phone
    })
    let saveuser = await add.save();
    console.log(saveuser)
    if (saveuser) {
        res.status(200).json({
            message: "User Successfully Added"
        })
    }
}

exports.get_all_user = async (req, res, next) => {
    let getUser = await userModel.find();
    if (getUser) {
        res.status(200).json(getUser)
    }
}

exports.delete_user = async (req, res, next) => {
    const body = req.body;

    let deleteUser = await userModel.findOneAndRemove({
        company_id: body.company_id
    });
    if (deleteUser) {
        res.status(200).json({
            message: "success"
        })
    }
}

exports.allocate_company = async (req, res, next) => {
    const body = req.body;
    let updateCompny = await userModel.findOneAndUpdate({
        _id: body._id
    }, {
        $set: {
            company_id: body.company_id,
            user_name: body.user_name,
            email: body.email,
            phone: body.phone
        }
    })


    return res.status(200).json({
        message: "User Successfully Update"
    })
}