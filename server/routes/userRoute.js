const express = require('express');
const router = express.Router();

const userController = require('../controller/userController');

router.post('/add_user', userController.ListUser);

router.get('/all_user', userController.get_all_user);

router.post('/delete_record', userController.delete_user);

router.patch('/allocate_user', userController.allocate_company);

module.exports = router;