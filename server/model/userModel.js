const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({

    company_id: {
        type: String
    },
    user_name: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: String
    }

});

module.exports = mongoose.model('User', userSchema);