const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Company = new Schema({
    _id: {
        type: String
    },

    Name: {
        type: String
    },
    City: {
        type: String
    }

});

module.exports = mongoose.model('Company', Company);